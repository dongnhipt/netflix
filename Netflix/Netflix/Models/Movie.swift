//
//  Movie.swift
//  Netflix
//
//  Created by Nhi Nguyễn Văn Đông on 26/03/2022.
//

import Foundation

struct TrendingMoviesResponse {
    let result: [Movie]
}

struct Movie {
    let id:Int
    let media_type: String?
    let original_name: String?
    let original_title: String?
    
}

/*
 
 {
adult = 0;
"backdrop_path" = "/iQFcwSGbZXMkeyKrxbPnwnRo5fl.jpg";
"genre_ids" =             (
 28,
 12,
 878
);
id = 634649;
"media_type" = movie;
"original_language" = en;
"original_title" = "Spider-Man: No Way Home";
overview = "Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.";
popularity = "6911.852";
"poster_path" = "/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg";
"release_date" = "2021-12-15";
title = "Spider-Man: No Way Home";
video = 0;
"vote_average" = "8.199999999999999";
"vote_count" = 10427;
},
 {
"backdrop_path" = "/m7FqiUOvsSk7Ulg2oRMfFGcLeT9.jpg";
"first_air_date" = "2020-12-25";
"genre_ids" =             (
 18
);
id = 91239;
"media_type" = tv;
name = Bridgerton;
"origin_country" =             (
 US
);
"original_language" = en;
"original_name" = Bridgerton;
overview = "Wealth, lust, and betrayal set in the backdrop of Regency era England, seen through the eyes of the powerful Bridgerton family.";
popularity = "405.518";
"poster_path" = "/luoKpgVwi1E5nQsi7W0UuKHu2Rq.jpg";
"vote_average" = "8.199999999999999";
"vote_count" = 1415;
},
 {
"backdrop_path" = "/vVKlL4HyrQYAcJuaaUW49FrRqY5.jpg";
"first_air_date" = "2022-03-25";
"genre_ids" =             (
 18
);
id = 110382;
"media_type" = tv;
name = Pachinko;
"origin_country" =             (
 KR
);
"original_language" = ko;
"original_name" = Pachinko;
overview = "Follow the hopes and dreams of four generations of a Korean immigrant family beginning with a forbidden love and crescendos into a sweeping saga that journeys between Korea, Japan and America to tell the unforgettable story of war and peace, love and loss, triumph and reckoning.";
popularity = "108.203";
"poster_path" = "/wUTXdmL6oNjhiStGveOaPeuFOYQ.jpg";
"vote_average" = "7.8";
"vote_count" = 5;
},
 */
