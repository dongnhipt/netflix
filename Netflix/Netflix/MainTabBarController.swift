//
//  ViewController.swift
//  Netflix
//
//  Created by Nhi Nguyễn Văn Đông on 23/03/2022.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let Home = UINavigationController(rootViewController: HomeViewController())
        let News = UINavigationController(rootViewController: News_HotViewController())
        let Laughts = UINavigationController(rootViewController: FastLaughtsViewController())
        let Searchs = UINavigationController(rootViewController: SearchsViewController())
        let Downloads = UINavigationController(rootViewController: DownloadsViewController())
        
        
        Home.tabBarItem.image = UIImage(systemName: "house")
        News.tabBarItem.image = UIImage(systemName: "play.rectangle.on.rectangle")
        Laughts.tabBarItem.image = UIImage(systemName: "face.smiling")
        Searchs.tabBarItem.image = UIImage(systemName: "magnifyingglass")
        Downloads.tabBarItem.image = UIImage(systemName: "arrow.down.to.line.circle")
        
        Home.title = "Home"
        News.title = "News & Hot"
        Laughts.title = "Fast Laughts"
        Searchs.title = "Searchs"
        Downloads.title = "Downloads"
        
        tabBar.tintColor = .label
        
        setViewControllers([Home, News, Laughts, Searchs, Downloads], animated: true)
    }


}

